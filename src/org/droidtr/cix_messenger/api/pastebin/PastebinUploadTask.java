package org.droidtr.cix_messenger.api.pastebin;

import java.util.*;
import org.blinksd.sdb.*;
import org.droidtr.cix_messenger.api.*;

public class PastebinUploadTask extends BaseTask {
	
	protected boolean encoded = false;

	@Override
	public String getOutput(String[] input) throws Throwable {
		Map<String, Object> args = new HashMap<>();
		/*
		args.put("user", "user");
		args.put("pass", "user");
		return getHTTPResult("/list.php", "POST", args);
		*/
		args.put("paste", encoded ? new String(Base64EXT.encode(input[0].getBytes(), Base64EXT.DEFAULT)) : input[0]);
		String result = getHTTPResult("/pastebin.php?plain", "POST", args);
		return result;
		/*
		args.put("user", "admin");
		args.put("pass", "admin");
		args.put("target", "user");
		String pasteHash = result.substring(result.indexOf("paste=")+6, result.lastIndexOf("\""));
		args.put("paste", pasteHash);
		return getHTTPResult("/sent.php", "POST", args);
		*/
	}

}
