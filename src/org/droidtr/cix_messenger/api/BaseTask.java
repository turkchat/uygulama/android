package org.droidtr.cix_messenger.api;

import android.os.*;
import java.util.*;
import org.frknkrc44.httplib.*;

public abstract class BaseTask extends AsyncTask<String, Void, TaskResult> {
  protected String baseURL = "https://paledega.sourceforge.io";

  public abstract String getOutput(String[] input) throws Throwable;
  
  public void setOnFinishListener(OnFinishListener finish) {
	  listener = finish;
  }

  @Override
  public TaskResult doInBackground(String[] input) {
    try {
      return TaskResult.create(true, getOutput(input));
    } catch(Throwable t) {
      return TaskResult.create(false, "EXCEPTION: " + ThrowablePrint.getTrace(t));
    }
  }
  
  @Override
  public void onPostExecute(TaskResult res) {
	  if(listener != null){
		  listener.onFinished(res);
	  }
  }
  
  private String result = null;
  
  protected String getHTTPResult(String link, String type, Map<String, Object> data) throws Throwable {
    if(!link.startsWith("http")){
		if(!link.startsWith("/")){
			link = "/" + link;
		}
		link = baseURL + link;
	}
    switch(type) {
	  case "GET":
	    GetRequest get = new GetRequest(link, data){
			@Override
			protected void onPostProcess(Map<String,List<String>> headers, String res) {
				result = res;
			}
		};
	    break;
	  case "POST":
	    PostMultipartFormRequest pr = new PostMultipartFormRequest(link, data){
			@Override
			protected void onPostProcess(Map<String,List<String>> headers, String res) {
				result = res;
			}
		};
		break;
	  default:
	    throw new RuntimeException("Unsupported type, use GET or POST!");
	}
	
	while(result == null){}
	return result;
  }
  
  private OnFinishListener listener;
  
  public static abstract class OnFinishListener {
	  public abstract void onFinished(TaskResult result);
  }
}
