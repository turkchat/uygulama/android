package org.droidtr.cix_messenger.api.messenger;

import java.util.*;
import org.blinksd.sdb.*;
import org.droidtr.cix_messenger.*;
import org.droidtr.cix_messenger.api.pastebin.*;

public class SendMessageTask extends PastebinUploadTask {
	
	public SendMessageTask() {
		super();
		encoded = true;
	}
	
	@Override
	public String getOutput(String[] input) throws Throwable {
		String result = super.getOutput(input);
		Map<String, Object> args = new HashMap<>();
		args.put("user", MessengerApplication.getUserName());
		args.put("pass", MessengerApplication.getPassword());
		args.put("target", input[1]);
		args.put("paste", result);
		result = getHTTPResult("/sent.php", "POST", args);
		return result;
	}
	
}
