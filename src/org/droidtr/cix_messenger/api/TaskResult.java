package org.droidtr.cix_messenger.api;

public class TaskResult {
  public final boolean result;
  public final String message;
  
  private TaskResult(boolean result, String message) {
    this.result = result;
    this.message = message;
  }
  
  public static TaskResult create(boolean result, String message){
    return new TaskResult(result, message);
  }
}
