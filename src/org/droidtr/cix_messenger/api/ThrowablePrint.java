package org.droidtr.cix_messenger.api;

import java.io.*;

public class ThrowablePrint {
  private ThrowablePrint(){}

  public static String getTrace(Throwable t) {
    String trace = "";
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(baos);
      t.printStackTrace(ps);
      trace = baos.toString();
      ps.close();
      baos.close();
    } catch(Throwable ignore){
      // do nothing
    }
    return trace;
  }
}
