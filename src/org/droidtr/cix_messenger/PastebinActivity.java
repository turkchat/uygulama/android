package org.droidtr.cix_messenger;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import org.droidtr.cix_messenger.api.*;
import org.droidtr.cix_messenger.api.pastebin.*;
import org.droidtr.cix_messenger.views.*;

public class PastebinActivity extends Activity implements View.OnClickListener {
  PastebinView pv;
	
  @Override
  public void onCreate(Bundle state) {
    super.onRestart();
    pv = new PastebinView(this);
    pv.setPasteButtonText("PASTE IT!");
    pv.setOnPasteButtonClick(this);
    setContentView(pv);
  }
  
  @Override
  public void onClick(final View v) {
	v.setEnabled(false);
	PastebinUploadTask task = new PastebinUploadTask();
	task.setOnFinishListener(new PastebinUploadTask.OnFinishListener(){
		@Override
		public void onFinished(TaskResult result){
			v.setEnabled(true);
			String paste = result.message;
			Toast.makeText(PastebinActivity.this, "Success: " + result.result + "\nMessage: " + paste, 1).show();
		}
	});
	task.execute(pv.getPasteText().toString());
  }
}
