package org.droidtr.cix_messenger;

import android.app.*;
import org.blinksd.sdb.*;

public class MessengerApplication extends Application {
	
	private static SuperMiniDB smdb;
	private static MessengerApplication app;
	
	@Override
	public void onCreate() {
		app = this;
		smdb = new SuperMiniDB(getPackageName(), getFilesDir());
		super.onCreate();
	}
	
	public static SuperMiniDB getDB() {
		return smdb;
	}
	
	public static MessengerApplication getApp() {
		return app;
	}
	
	public static String getUserName() {
		return getDB().getString("current_username", "a");
	}
	
	public static String getPassword() {
		return getDB().getString("current_password", "a");
	}
	
	public static void setUserName(String userName) {
		SuperMiniDB db = getDB();
		String key = "current_username";
		db.putString(key, userName);
		db.refreshKey(key);
	}
	
	public static void setPassword(String password) {
		SuperMiniDB db = getDB();
		String key = "current_password";
		db.putString(key, password);
		db.refreshKey(key);
	}
}
