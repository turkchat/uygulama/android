package org.droidtr.cix_messenger.views;

import android.content.*;
import android.view.*;
import android.widget.*;
import static android.view.Gravity.*;

public class PastebinView extends LinearLayout {
	
	EditText pasteText;
	Button sendPasteButton;

	public PastebinView(Context ctx){
		super(ctx);
		createPastebinView();
	}

	private void createPastebinView() {
		setOrientation(VERTICAL);
		pasteText = new EditText(getContext());
		PastebinView.LayoutParams editTextParams = new PastebinView.LayoutParams(-1,-1,1);
		pasteText.setLayoutParams(editTextParams);
		pasteText.setGravity(TOP | START);
		addView(pasteText);
		sendPasteButton = new Button(getContext());
		PastebinView.LayoutParams buttonParams = new PastebinView.LayoutParams(-1,-2,0);
		sendPasteButton.setLayoutParams(buttonParams);
		addView(sendPasteButton);
	}
	
	public void setPasteText(CharSequence text) {
		pasteText.setText(text);
	}
	
	public CharSequence getPasteText() {
		return pasteText.getText();
	}
	
	public void setOnPasteButtonClick(View.OnClickListener listener) {
		sendPasteButton.setOnClickListener(listener);
	}
	
	public void setPasteButtonText(CharSequence text) {
		sendPasteButton.setText(text);
	}
	
	public CharSequence getPasteButtonText() {
		return sendPasteButton.getText();
	}

}
