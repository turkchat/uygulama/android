package org.droidtr.cix_messenger.views;

import android.content.*;
import android.view.*;
import android.widget.*;
import static android.view.Gravity.*;

public class MessengerView extends LinearLayout {
	
	EditText pasteText;
	Button sendPasteButton;

	public MessengerView(Context ctx){
		super(ctx);
		createMessengerView();
	}

	private void createMessengerView() {
		setOrientation(VERTICAL);
		pasteText = new EditText(getContext());
		MessengerView.LayoutParams editTextParams = new MessengerView.LayoutParams(-1,-1,1);
		pasteText.setLayoutParams(editTextParams);
		pasteText.setGravity(TOP | START);
		addView(pasteText);
		sendPasteButton = new Button(getContext());
		MessengerView.LayoutParams buttonParams = new MessengerView.LayoutParams(-1,-2,0);
		sendPasteButton.setLayoutParams(buttonParams);
		addView(sendPasteButton);
	}
	
	public void setPasteText(CharSequence text) {
		pasteText.setText(text);
	}
	
	public CharSequence getPasteText() {
		return pasteText.getText();
	}
	
	public void setOnPasteButtonClick(View.OnClickListener listener) {
		sendPasteButton.setOnClickListener(listener);
	}
	
	public void setPasteButtonText(CharSequence text) {
		sendPasteButton.setText(text);
	}
	
	public CharSequence getPasteButtonText() {
		return sendPasteButton.getText();
	}

}
