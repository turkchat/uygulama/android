package org.frknkrc44.httplib;

import java.io.*;
import java.net.*;
import java.util.*;

public abstract class PostFormRequest extends BaseRequest {
	
	public PostFormRequest(String link, Map<String, Object> args) throws IOException {
		super(link, args);
	}

	@Override
	protected void onProcess(String link, Map<String, Object> args) throws IOException {
		HttpURLConnection conn = getConnector(link);
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		String arg = parseMethodArgs(args);
		byte[] argBytes = arg.getBytes();
		conn.setRequestProperty("Content-Length",String.valueOf(argBytes.length));
		conn.connect();
		OutputStream os = conn.getOutputStream();
		os.write(argBytes);
		os.close();
		onPostProcess(conn.getHeaderFields(), getResult(conn, getCharset(args)));
	}
	
}
