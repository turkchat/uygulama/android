package org.frknkrc44.httplib;

import java.io.*;
import java.net.*;
import java.util.*;

public abstract class PostMultipartFormRequest extends BaseRequest {
	
	public PostMultipartFormRequest(String link, Map<String, Object> args) throws IOException {
		super(link, args);
	}
	
	@Override
	protected void onProcess(String link, Map<String, Object> args) throws IOException {
		assert(args != null && args.size() > 0) : "args null or empty";
		HttpURLConnection conn = getConnector(link);
		
		final String hyphens = "--", end = "\r\n", boundary = createBoundary();
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+boundary);
		conn.setDoOutput(true);
		
		List<String> keys = new ArrayList<String>(args.keySet());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for(String key : keys){
			Object obj = args.get(key);
			pushBytes(baos, hyphens + boundary + end);
			if(obj instanceof File){
				File fObj = (File) obj;
				pushBytes(baos, "Content-Disposition: form-data; name=\""+key+"\";filename=\""+fObj.getName()+"\"" + end); 
				pushBytes(baos, "Content-Type: application/octet-stream" + end);
				pushBytes(baos, "Content-Transfer-Encoding: binary" + end);
				pushBytes(baos, end);
				pushFileBytes(baos, fObj);
				pushBytes(baos, end);
			} else {
				pushBytes(baos, "Content-Disposition: form-data; name=\""+key+"\"" + end);
				pushBytes(baos, "Content-Type: text/plain; charset=UTF-8" + end);
				pushBytes(baos, "Content-Transfer-Encoding: 8bit" + end);
				pushBytes(baos, end);
				pushBytes(baos, obj + end);
			}
		}
		pushBytes(baos, hyphens + boundary + hyphens + end); 
		
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		byte[] buf = new byte[1024];
		int read;
		
		conn.connect();
		
		OutputStream os = conn.getOutputStream();
		while((read = bais.read(buf,0,buf.length)) > 0){
			os.write(buf, 0, read);
			os.flush();
		}
		os.close();
		bais.close();
		baos.close();
		
		onPostProcess(conn.getHeaderFields(), getResult(conn,getCharset(args)));
	}
	
	private void pushBytes(ByteArrayOutputStream baos, String str) throws IOException {
		baos.write(str.getBytes());
		baos.flush();
	}
	
	private void pushFileBytes(ByteArrayOutputStream baos, File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		byte[] buf = new byte[1024];
		int read;
		while((read = fis.read(buf,0,buf.length)) > 0){
			baos.write(buf,0,read);
		}
		fis.close();
		baos.flush();
	}
	
	private static String createBoundary(){
		String bound = "";
		for(int i = 0;i < 20;i++){
			bound += Integer.toHexString((int)(System.currentTimeMillis() % 16));
		}
		return bound;
	}
}
