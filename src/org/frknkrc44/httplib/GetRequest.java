package org.frknkrc44.httplib;

import java.io.*;
import java.net.*;
import java.util.*;

public abstract class GetRequest extends BaseRequest {
	
	public GetRequest(String link, Map<String, Object> args) throws IOException {
		super(link, args);
	}

	@Override
	public void onProcess(String link, Map<String, Object> args) throws IOException {
		String arg = parseMethodArgs(args);
		arg = arg.length() > 0 ? ("?" + arg) : "";
		HttpURLConnection conn = getConnector(link + arg);
		conn.connect();
		onPostProcess(conn.getHeaderFields(), getResult(conn,getCharset(args)));
	}
	
}
