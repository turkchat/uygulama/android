package org.frknkrc44.httplib;

import java.io.*;
import java.net.*;
import java.util.*;

public abstract class PostDataRequest extends BaseRequest {

	public PostDataRequest(String link, byte[] data, String fileType) throws IOException {
		super(link, getDataMap(data,fileType));
	}
	
	public PostDataRequest(String link, byte[] data) throws IOException {
		this(link, data, null);
	}
	
	private static Map<String, Object> getDataMap(byte[] data, String fileType){
		Map<String,Object> out = new HashMap<>();
		out.put("data",data);
		out.put("type",fileType != null ? fileType : "text/plain");
		return out;
	}
	
	private boolean dataValid(Map<String,Object> args){
		return args != null && args.containsKey("data") &&
			args.get("data") != null && args.get("data") instanceof byte[];
	}

	@Override
	protected void onProcess(String link, Map<String, Object> args) throws IOException {
		assert(dataValid(args)) : "where is the data?";
		HttpURLConnection conn = getConnector(link);
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type",(String) args.get("type"));
		byte[] argBytes = (byte[]) args.get("data");
		conn.setRequestProperty("Content-Length",String.valueOf(argBytes.length));
		conn.connect();
		OutputStream os = conn.getOutputStream();
		os.write(argBytes);
		os.close();
		onPostProcess(conn.getHeaderFields(), getResult(conn, getCharset(args)));
	}

}
